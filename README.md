# README #
This is a repository which contains python based code to classify cystine stabilized peptide toxins based on their functional characteristics. 



### What is this repository for? ###
* Summary: 
Disulfide rich small peptide toxins offer an array of functional characteristics being very stable at the same time. This special feature
make them very attractive candidate as peptide drug scaffold. Although these small peptide toxin are broadly distributed throughtout a wide 
source organisms, only a small fraction of this type of peptides are discorvered and characterized. With a small chainsize and high diversity
in the primary sequences made it further difficult to discover these peptide using sequence alignment software like BLAST. Here, we offer a 
natural language processing based supervised machine learning algorithm to predict/classify the most common functional classes disulfide
rich peptide toxins: 1) Ion channel blocker, 2) Antimicrobial peptide, 3)Acetylcholine receptor inhibitor, 4)Serine protease inhibitor, and 
Hemolytic/Cytoxic peptides. We use a modified n-gram and skip-gram model to generate important features from the primary sequences and evantualy 
used that feature vector for a Logistic Regression classifier. We hope our software will benefit the rearch community to discover new 
disulfide rich peptide toxin with a specific functional identity.  

* Version:
This is starting version of this software. The version is 1.1



### How do I get set up? ###

* Summary of set up
To use this software one just need to install python 2.7 or 3.3 in the system.

* Configuration
All the files and folders have to downloaded in the local machine and python should be run from that same directory where files and folders 
are downloaded. 

* Dependencies
The user needs to install biopython, scikit-learn, numpy, pandas and scipy libraries using pip, conda, miniconda or easy setup

* How to run tests
To run CSP (Cystine Stabilized Peptide toxins) file, one should be in the original directory where all the file and folders are 
downloaded. Then following command has to be entered:
"python predcsp.py test.fasta" 
where, test.fasta is a fasta file that contains the unknown peptide sequences. As the program execution is completed, that will generate
a .csv file which show the probability of five different funcitonal characteristics of the sample peptide chain. 



### Who do I talk to? ###

* Repo owner or admin
If you have any question regarding to this program or need anyhelp to run code, please contact S M Ashiqul Islam.
emai: s_islam@baylor.edu
