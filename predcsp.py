# -*- coding: utf-8 -*-
"""
Created on Sun Jun 18 21:21:30 2017

@author: Ashiqul
"""


import csp_prediction as csp
from sys import argv
script, test = argv
fh = open(test)
a,b,c,d,e,f,g = csp.server_for_csp(fh)

result = open("result.csv", "w")
result.write("Protein id, Ion channel blocker probability, AMP probability, Acetylcholine receptor inhibitor probability, Serine protease inhibitor probability, Hemolytic activity probability, Protein sequence \n")
for a1,b1,c1,d1,e1,f1,g1 in zip(a,b,c,d,e,f,g):
    result.write(str(a1)+","+str(c1[1])+","+str(d1[1])+","+str(e1[1])+","+str(f1[1])+","+str(g1[1])+","+str(b1)+"\n")
fh.close()
result.close()
