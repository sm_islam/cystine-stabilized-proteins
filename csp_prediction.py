from __future__ import division
from sklearn.model_selection import LeaveOneOut
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.feature_extraction.text import CountVectorizer
from Bio import SeqIO
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit
from sklearn.linear_model import LogisticRegression
import pickle
import os

import feature_module as fm 


def predict(sample, clfobj, voc, p2=10, p4=10, p5=2, p6=2, p7=5, p8=1):
    
    
    sample.seek(0)   
    corpus2=[]#make a blank list of corpus for the training set
        
    for line in SeqIO.parse(sample, "fasta"):
        line = str(line.seq)
        line = line.lower().replace("x","")
        line = line.replace("hhhhhh", "")
        line = line.replace('-', "")
           # print line
            
        fullstring = fm.extract_motifs_pos(line,  1, p2, 1, p4, p5, p6, p7, p8)
        #fullstring = fullstring+ " "+ pos_prot_1st_word(line)
        corpus2.append(fullstring) #apperd string from each protein to corpus
        
        
        
    corpus = np.array(corpus2) #convert corpus into numpy array  
    #print corpus # print for debugging 
    #print tag # print for debugging
    
    count = CountVectorizer(vocabulary = voc)
    #get the vocabulary of train set to use for the test set
    
    
    
    bag_test = count.fit_transform(corpus) #transform the corpus(bag of words into sparse martrix)
    bag_test= bag_test.toarray()
    np.place(bag_test, bag_test>0, [1])
    #bag_test = pca.transform(bag_test)
    
    x_test = bag_test
    #y_test = tag
    # fit the Logistic Regression model
   
    y_pred= clfobj.predict(x_test)
    
    y_prob =clfobj.predict_proba(x_test)
    
    #print y_pred
    
    return y_pred, y_prob

     

    
def is_ion(sample):
    with open("temp/"+"ion"+"voc.file", "rb") as f:
        voc = pickle.load(f)
    with open("temp/"+"ion"+"cljobj.file", "rb") as f:
        clf = pickle.load(f)
    with open("temp/"+"ion"+"par.file", "rb") as f:
        par = pickle.load(f)
    
    y_pred, y_prob = predict(sample, clf, voc, p2=par[0], p4=par[1], p5=par[2], p6=par[3], p7=par[4], p8=par[5])
    return y_pred, y_prob
def is_amp(sample):
    with open("temp/"+"amp"+"voc.file", "rb") as f:
        voc = pickle.load(f)
    with open("temp/"+"amp"+"cljobj.file", "rb") as f:
        clf = pickle.load(f)
    with open("temp/"+"amp"+"par.file", "rb") as f:
        par = pickle.load(f)
    
    y_pred, y_prob = predict(sample, clf, voc, p2=par[0], p4=par[1], p5=par[2], p6=par[3], p7=par[4], p8=par[5])
    return y_pred, y_prob
def is_achr(sample):
    with open("temp/"+"achr"+"voc.file", "rb") as f:
        voc = pickle.load(f)
    with open("temp/"+"achr"+"cljobj.file", "rb") as f:
        clf = pickle.load(f)
    with open("temp/"+"achr"+"par.file", "rb") as f:
        par = pickle.load(f)
    
    y_pred, y_prob = predict(sample, clf, voc, p2=par[0], p4=par[1], p5=par[2], p6=par[3], p7=par[4], p8=par[5])
    return y_pred, y_prob
def is_serine(sample):
    with open("temp/"+"serine"+"voc.file", "rb") as f:
        voc = pickle.load(f)
    with open("temp/"+"serine"+"cljobj.file", "rb") as f:
        clf = pickle.load(f)
    with open("temp/"+"serine"+"par.file", "rb") as f:
        par = pickle.load(f)
    
    y_pred, y_prob = predict(sample, clf, voc, p2=par[0], p4=par[1], p5=par[2], p6=par[3], p7=par[4], p8=par[5])
    return y_pred, y_prob
def is_hemo(sample):
    with open("temp/"+"hemo"+"voc.file", "rb") as f:
        voc = pickle.load(f)
    with open("temp/"+"hemo"+"cljobj.file", "rb") as f:
        clf = pickle.load(f)
    with open("temp/"+"hemo"+"par.file", "rb") as f:
        par = pickle.load(f)
    
    y_pred, y_prob = predict(sample, clf, voc, p2=par[0], p4=par[1], p5=par[2], p6=par[3], p7=par[4], p8=par[5])
    return y_pred, y_prob
    
def seq_predictor(line):
    a1, a2 = is_ion(line)
    b1, b2 = is_amp(line)
    c1, c2 = is_achr(line)
    d1, d2 = is_serine(line)
    e1, e2 = is_hemo(line)
    return a1, a2, b1, b2, c1, c2, d1, d2, e1, e2


###################################################################################
###################################################################################
###################################################################################
###################################################################################

def server_for_csp(f):
    
    tag= seq_predictor(f)
    a2 = tag[1]   
    b2 = tag[3]
    c2 = tag[5]
    d2 = tag[7]
    e2 = tag[9]
    
    
    
    
       
    ids = []
    seqs = []
    
    
    
    
    f.seek(0)  
    n=0
    for lines in SeqIO.parse(f, "fasta"):
        ids.append(str(lines.id))
        line = str(lines.seq)
        seqs.append(line.upper())
        line = line.upper()
        
        if line.count("C")<2 or len(line)<13:
            a2[n][1]=0.0
            b2[n][1]=0.0
            c2[n][1]=0.0
            d2[n][1]=0.0
            e2[n][1]=0.0
        else:
            a2[n][1]=round(a2[n][1],2)
            b2[n][1]=round(b2[n][1],2)
            c2[n][1]=round(c2[n][1],2)
            d2[n][1]=round(d2[n][1],2)
            e2[n][1]=round(e2[n][1],2)
            
        
        n=n+1
    return ids,seqs, a2,b2,c2,d2,e2

    
  
    
    
""" 
for a,b,i,j,k,l,m in zip(ids,seqs, a2, b2, c2, d2, e2):
    print ("Protein ID:", a, "Sequence:", b, 
           "prob of ion_channel_blocker:", round(i[1], 2), "prob of amp:", round(j[1], 2),
           "prob of achr:", round(k[1], 2), "prob of serine protease inhibitor:", round(l[1],2),
           "prob of hemolytic pepide:", round(m[1],2))
"""  

